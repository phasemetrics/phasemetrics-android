package io.phasemetrics.phasemetrics.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.annotation.Nullable;

import io.phasemetrics.phasemetrics.agents.PluginManagerAgent;
import io.phasemetrics.phasemetrics.agents.SupervisorAgent;
import io.phasemetrics.phasemetrics.misc.Installation;
import jade.android.AndroidHelper;
import jade.android.MicroRuntimeService;
import jade.android.MicroRuntimeServiceBinder;
import jade.android.RuntimeCallback;
import jade.core.MicroRuntime;
import jade.core.Profile;
import jade.util.leap.Properties;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;

/**
 * Created by sando on 4/20/16.
 */
public class JADEService extends Service {

    private MicroRuntimeServiceBinder microRuntimeServiceBinder;
    private ServiceConnection serviceConnection;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startJADEService();
        return START_STICKY;
    }

    private RuntimeCallback<AgentController> agentStartupCallback = new RuntimeCallback<AgentController>() {
        @Override
        public void onSuccess(AgentController agentController) {
        }

        @Override
        public void onFailure(Throwable throwable) {
        }
    };

    public void startJADEService() {
        // Build properties.
        final Properties profile = new Properties();
        profile.setProperty(Profile.MAIN_HOST, "139.162.185.31");
        profile.setProperty(Profile.MAIN_PORT, "1099");
        profile.setProperty(Profile.MAIN, Boolean.FALSE.toString());
        profile.setProperty(Profile.JVM, Profile.ANDROID);
        profile.setProperty(Profile.LOCAL_HOST, AndroidHelper.getLocalIPAddress());

        // Bind service.
        if (this.microRuntimeServiceBinder == null) {
            serviceConnection = new ServiceConnection(){
                public void onServiceConnected(ComponentName className, IBinder service) {
                    microRuntimeServiceBinder = (MicroRuntimeServiceBinder) service;
                    startContainer(profile);
                }

                public void onServiceDisconnected(ComponentName className) {
                    microRuntimeServiceBinder = null;
                }
            };
            bindService(new Intent(getApplicationContext(), MicroRuntimeService.class),
                    serviceConnection, Context.BIND_AUTO_CREATE);
        }
        else {
            startContainer(profile);
        }
    }

    private void startContainer(Properties profile) {
        if (!MicroRuntime.isRunning()) {
            microRuntimeServiceBinder.startAgentContainer(profile, new RuntimeCallback<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    startAgents(agentStartupCallback);
                }

                @Override
                public void onFailure(Throwable throwable) {

                }
            });
        }
        else {
            startAgents(agentStartupCallback);
        }
    }

    private void startAgents(final RuntimeCallback<AgentController> aSC) {
        String devId = Installation.id(getApplicationContext());
        final String supervisorName = devId.concat("-SA");
        final String pluginManagerName = devId.concat("-PMA");

        // Start Supervisor Agent
        microRuntimeServiceBinder.startAgent(supervisorName,
                SupervisorAgent.class.getName(),
                new Object[]{getApplicationContext()},
                new RuntimeCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        try {
                            aSC.onSuccess(MicroRuntime.getAgent(supervisorName));
                        }
                        catch (ControllerException e) {
                            aSC.onFailure(e);
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) {
                        aSC.onFailure(throwable);
                    }
                });

        // Start Plugin Manager Agent
        microRuntimeServiceBinder.startAgent(pluginManagerName,
                PluginManagerAgent.class.getName(),
                new Object[]{getApplicationContext()},
                new RuntimeCallback<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        try {
                            aSC.onSuccess(MicroRuntime.getAgent(pluginManagerName));
                        }
                        catch (ControllerException e) {
                            aSC.onFailure(e);
                        }
                    }

                    @Override
                    public void onFailure(Throwable throwable) { aSC.onFailure(throwable); }
                });
    }
}
