package io.phasemetrics.phasemetrics.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import io.phasemetrics.phasemetrics.services.JADEService;

/**
 * Created by sando on 4/20/16.
 */
public class StartJADEServiceOnBootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            Intent serviceIntent = new Intent(context, JADEService.class);
            context.startService(serviceIntent);
        }
    }
}
